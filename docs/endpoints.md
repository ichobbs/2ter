APIs

Accounts
Accounts are user-created accounts used for the app. It will provide authentication to most of the features included in our app.


Method: POST


Path: /api/accounts


JSON request body:
Input:

{
  "account_id": 0,
  "username": "string",
  "email": "string",
  "password": "string",
  "first_name": "string",
  "last_name": "string"
}


Output:

{
  "username": "string",
  "email": "string",
  "hashed_password": "$2b$12$bQJgEtjQMJlTT1MBYN74O.s99BG2WnoJXVSm31hYMek0TtrviKaLW"
}


Creating a new account saves first name, last name, email, user name, password, and hashed password to the database.This adds a new account to database and returns the account information.


Method: GET


Path: /api/accounts


JSON request body:
Output:

[
  {
    "account_id": 0,
    "username": "string",
    "email": "string",
  }
]


Get request would return a list of all accounts. It would show the account_id, username and email of all the accounts stored in the database.


Method: GET


Path: /api/accounts/{pk}


JSON request body:
Output:

{
    "account_id": 0,
    "username": "string",
    "email": "string",
    "hashed_password": "$2b$12$bQJgEtjQMJlTT1MBYN74O.s99BG2WnoJXVSm31hYMek0TtrviKaLW"
}


Get request for a specific account will return account data of the account associated with that id number. It will return the account_id, email, username, and a hashed_password for that specific account. You need to have an authorized token to do this GET request.


Method: PUT


Path: /api/accounts/{pk}


JSON request body:
Input:

{
  "email": "string",
  "username": "string",
  "password": "string"
}


Output:

{
  "email": "string",
  "username": "string",
  "hashed_password": "string"
}


A PUT request will allow users to make changes to their current account data. They are able to make changes to their first_name, last_name, email, user_name, and password. The data will return their first_name, last_name, email, user_name, and a hashed_password.


Method: DELETE


Path: /api/accounts/{pk}


JSON request body:
Output:

true


A DELETE request will delete that account from the database.


Events
POST creates new events
POST /events
Authorization: Bearer <authentication_token>
Content-Type: application/json

{
    "topic": "Team Meeting",
    "author": 1,
    "partner": 2,
    "paired": true,
    "expired": false,
    "created_at": "2023-09-06T14:00:00Z",
    "scheduled_at": "2023-09-10T10:00:00Z",
    "zoom_link": "https://zoom.us/meeting/12345",
    "category": "Work"
}

{
    "event_id": 123,
    "topic": "Team Meeting",
    "author": 1,
    "partner": 2,
    "paired": true,
    "expired": false,
    "created_at": "2023-09-06T14:00:00Z",
    "scheduled_at": "2023-09-10T10:00:00Z",
    "zoom_link": "https://zoom.us/meeting/12345",
    "category": "Work"
}

With PUT, you can update events
PUT /events/123
Authorization: Bearer <authentication_token>
Content-Type: application/json

{
    "topic": "Updated Team Meeting",
    "scheduled_at": "2023-09-11T11:00:00Z",
    "category": "Work"
}

{
    "event_id": 123,
    "topic": "Updated Team Meeting",
    "author": 1,
    "partner": 2,
    "paired": true,
    "expired": false,
    "created_at": "2023-09-06T14:00:00Z",
    "scheduled_at": "2023-09-11T11:00:00Z",
    "zoom_link": "https://zoom.us/meeting/12345",
    "category": "Work"
}

Profiles
POST /profiles
Content-Type: application/json

{
    "account": 123,
    "first_name": "John",
    "last_name": "Doe",
    "skills": "Web Development",
    "interests": "Gaming, Hiking",
    "bio": "Passionate web developer with a love for nature and gaming."
}

{
    "profile_id": 456,
    "account": 123,
    "first_name": "John",
    "last_name": "Doe",
    "skills": "Web Development",
    "interests": "Gaming, Hiking",
    "bio": "Passionate web developer with a love for nature and gaming."
}


PUT /profiles/456
Content-Type: application/json

{
    "skills": "Updated Web Development Skills",
    "bio": "Updated bio information."
}


{
    "profile_id": 456,
    "account": 123,
    "first_name": "John",
    "last_name": "Doe",
    "skills": "Updated Web Development Skills",
    "interests": "Gaming, Hiking",
    "bio": "Updated bio information."
}


Reviews
POST /reviews
Content-Type: application/json

{
    "author": 123,
    "reviewee": 456,
    "date": "2023-09-06T14:00:00Z",
    "rating": 5,
    "review_text": "Excellent service provided by the user."
}

{
    "review_id": 789,
    "author": 123,
    "reviewee": 456,
    "date": "2023-09-06T14:00:00Z",
    "rating": 5,
    "review_text": "Excellent service provided by the user."
}

GET /reviews/789

{
    "review_id": 789,
    "author": 123,
    "reviewee": 456,
    "date": "2023-09-06T14:00:00Z",
    "rating": 5,
    "review_text": "Excellent service provided by the user."
}

PUT /reviews/789
Content-Type: application/json

{
    "rating": 4,
    "review_text": "Good service provided."
}

{
    "review_id": 789,
    "author": 123,
    "reviewee": 456,
    "date": "2023-09-06T14:00:00Z",
    "rating": 4,
    "review_text": "Good service provided."
}

DELETE /reviews/789

{
    "message": "Review with ID 789 has been deleted."
}


Messages
POST /messages
Content-Type: application/json

{
    "sender": 123,
    "receiver": 456,
    "date": "2023-09-06T14:00:00Z",
    "subject": "Important Update",
    "body": "Please review the attached document for the latest updates."
}

{
    "message_id": 789,
    "sender": 123,
    "receiver": 456,
    "date": "2023-09-06T14:00:00Z",
    "subject": "Important Update",
    "body": "Please review the attached document for the latest updates."
}


GET /messages/789

{
    "message_id": 789,
    "sender": 123,
    "receiver": 456,
    "date": "2023-09-06T14:00:00Z",
    "subject": "Important Update",
    "body": "Please review the attached document for the latest updates."
}


PUT /messages/789
Content-Type: application/json

{
    "subject": "Updated Information",
    "body": "Please disregard the previous message. Here's the updated information."
}


{
    "message_id": 789,
    "sender": 123,
    "receiver": 456,
    "date": "2023-09-06T14:00:00Z",
    "subject": "Updated Information",
    "body": "Please disregard the previous message. Here's the updated information."
}

DELETE /messages/789

{
    "message": "Message with ID 789 has been deleted."
}

DELETE allows you to delete a message.
