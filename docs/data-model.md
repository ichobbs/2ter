Accounts Model

Attributes:
account_id (int): The unique identifier of the account.
email (str): The email address associated with the account.
username (str): The username associated with the account.
password (str): The hashed password for the account.



Messages Model

Attributes:
message_id (int): The unique identifier of the message.
sender (int): The ID of the sender of the message.
receiver (int): The ID of the receiver of the message.
date (datetime): The date and time when the message was sent.
subject (str): The subject or title of the message.
body (str): The content of the message.



Reviews Model

Attributes:
review_id (int): The unique identifier of the review.
author (int): The ID of the author of the review.
reviewee (int): The ID of the user being reviewed.
date (datetime): The date and time when the review was created.
rating (int): The rating given in the review.
review_text (str): The text content of the review.



Profiles Model

Attributes:
profile_id (int): The unique identifier of the profile.
account (int): The ID of the associated account.
picture_url (str): The URL of the profile picture.
first_name (str): The first name of the user.
last_name (str): The last name of the user.
skills (str): A description of the user's skills.
interests (str): A description of the user's interests.
bio (str): A bio or summary of the user.



Events Model

Attributes:
event_id (int): The unique identifier of the event.
topic (str): The topic or title of the event.
author (int): The ID of the event's author or creator.
partner (int): The ID of the event's partner or co-host.
paired (bool): A flag indicating whether the event is paired.
expired (bool): A flag indicating whether the event has expired.
created_at (datetime): The date and time when the event was created.
scheduled_at (datetime): The date and time when the event is scheduled.
zoom_link (str): The Zoom link associated with the event.
category (str): The category or type of the event.
