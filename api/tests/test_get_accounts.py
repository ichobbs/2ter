# Test conducted by Felix Mogire

from fastapi.testclient import TestClient
from main import app
from queries.accounts import AccountsRepository


client = TestClient(app)


class EmptyAccountRepository:
    def get_all(self):
        return [
            {
                "account_id": 1,
                "username": "test",
                "email": "test@email.com",
            },
        ]


def test_get_all_accounts():
    app.dependency_overrides[AccountsRepository] = EmptyAccountRepository
    response = client.get("/api/accounts")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == [
        {
            "account_id": 1,
            "username": "test",
            "email": "test@email.com",
        }
    ]
