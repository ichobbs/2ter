from fastapi.testclient import TestClient
from main import app
from queries.messages import MessageRepository

client = TestClient(app)


class EmptyMessageRepository:
    def get_all(self):
        return [
            {
                "message_id": 1,
                "sender": 1,
                "receiver": 2,
                "date": "2023-09-01T12:00:00Z",
                "subject": "Hello",
                "body": "Test message body",
            }
        ]


def test_get_all_messages():
    app.dependency_overrides[MessageRepository] = EmptyMessageRepository
    response = client.get("/messages")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["message_id"] == 1
    assert response.json()[0]["subject"] == "Hello"
