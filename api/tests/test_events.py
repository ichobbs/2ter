from fastapi.testclient import TestClient
from main import app
from queries.events import EventRepository
from datetime import datetime

client = TestClient(app)


class EmptyEventRepository:
    def get_all(self):
        return [
            {
                "event_id": 1,
                "topic": "Test Event",
                "author": 1,
                "partner": 2,
                "paired": False,
                "expired": False,
                "created_at": datetime.now(),
                "scheduled_at": datetime.now(),
                "zoom_link": "https://example.com",
                "category": "Test Category",
            }
        ]

    def get_one(self, event_id: int):
        return {
            "event_id": 1,
            "topic": "Test Event",
            "author": 1,
            "partner": 2,
            "paired": False,
            "expired": False,
            "created_at": datetime.now(),
            "scheduled_at": datetime.now(),
            "zoom_link": "https://example.com",
            "category": "Test Category",
        }


def test_get_all_events():
    app.dependency_overrides[EventRepository] = EmptyEventRepository
    response = client.get("/events")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0]["event_id"] == 1
    assert response.json()[0]["topic"] == "Test Event"
