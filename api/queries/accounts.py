from queries.pool import pool
from typing import List, Union
from models.accounts import (
    Account,
    AccountsIn,
    AccountsOut,
    Error,
    AuthenticationException,
    AccountOutPUT,
    AccountPUT,
    AccountToken,
)


class AccountsRepository:
    def get_all(self) -> Union[Error, List[Account]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT account_id,
                            email,
                            username
                        FROM accounts;
                        """
                    )
                    result = []
                    for record in db:
                        account = Account(
                            account_id=record[0],
                            email=record[1],
                            username=record[2],
                        )
                        result.append(account)

                    return result
        except Exception as e:
            return {"message": e}

    def create(self, accounts: Account, hashed_password: str) -> int:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO accounts (
                        email,
                        username,
                        password
                        )
                    VALUES (%s, %s, %s)
                    RETURNING account_id;
                    """,
                    (
                        accounts.email,
                        accounts.username,
                        hashed_password,
                    ),
                )
                pk = result.fetchone()[0]
                return pk

    def delete(self, pk: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM accounts
                        WHERE account_id = %s;
                        """,
                        (pk,),
                    )
                    return True
        except Exception as e:
            return {False: e}

    def get_account_by_id(self, pk: int) -> AccountsOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT account_id, email, username, password
                    FROM accounts
                    WHERE account_id = %s;
                    """,
                    [pk],
                )
                ac = cur.fetchone()
                if ac is None:
                    raise AuthenticationException("No account found")
                else:
                    try:
                        return self.record_to_account_out(ac)
                    except Exception as e:
                        raise Exception("Error:", e)

    def get_account_by_username(self, username: str) -> AccountsOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT account_id, email, username, password
                    FROM accounts
                    WHERE username = %s;
                    """,
                    [username],
                )
                ac = cur.fetchone()
                if ac is None:
                    raise AuthenticationException("No account found")
                else:
                    try:
                        return self.record_to_account_out(ac)
                    except Exception as e:
                        raise Exception("Error:", e)

    def get_account(self, username: str) -> AccountsOut | None:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT account_id,
                            email,
                            username,
                            password
                          FROM accounts
                          WHERE username = %s;
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        raise Exception("No account found")
                    else:
                        try:
                            return self.record_to_account_out(record)
                        except Exception as e:
                            raise Exception("Error:", e)
        except Exception as e:
            raise Exception("Error:", e)

    def record_to_account_out(self, record):
        return AccountsOut(
            account_id=record[0],
            email=record[1],
            username=record[2],
            hashed_password=record[3],
        )

    def update(
        self,
        account_id: int,
        accounts: AccountPUT,
        hashed_password: str,
    ) -> Union[AccountOutPUT, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE accounts
                        SET email = %s,
                            username = %s,
                            password = %s
                        WHERE account_id = %s
                        RETURNING
                        email,
                        username;
                        """,
                        [
                            accounts.email,
                            accounts.username,
                            hashed_password,
                            account_id,
                        ],
                    )
                # account_id = result.fetchone()[0]
                # return self.account_in_to_out(account_id, accounts)
                return AccountOutPUT(
                    email=accounts.email,
                    username=accounts.username,
                    hashed_password=hashed_password,
                )
        except Exception as e:
            print(e, "-------------------------------")
            return Error(str(e))

    def account_in_to_out(self, account_id: int, accounts: AccountsIn):
        old_data = accounts.dict()
        return AccountsOut(account_id=account_id, **old_data)

    def doNothing(AccountToken: AccountToken):
        pass
