from pydantic import BaseModel
from datetime import datetime
from queries.pool import pool
from typing import List, Union, Optional


# Models #
class Error(BaseModel):
    message: str


class EventIn(BaseModel):
    topic: str
    author: int
    partner: int
    paired: bool
    expired: bool
    created_at: datetime
    scheduled_at: datetime
    zoom_link: str
    category: str


class EventOut(BaseModel):
    event_id: int
    topic: str
    author: int
    partner: int
    paired: bool
    expired: bool
    created_at: datetime
    scheduled_at: datetime
    zoom_link: str
    category: str


class RequestedEventOut(BaseModel):
    event_id: int
    topic: str
    author: int
    author_first_name: str
    author_last_name: str
    partner: int
    paired: bool
    expired: bool
    created_at: datetime
    scheduled_at: datetime
    zoom_link: str


class UpdateEventToPaired(BaseModel):
    event_id: int


class UpdateEventToExpired(BaseModel):
    event_id: int


class PairedEventOut(BaseModel):
    event_id: int
    topic: str
    author: int
    author_first_name: str
    author_last_name: str
    partner: int
    partner_first_name: str
    partner_last_name: str
    paired: bool
    expired: bool
    created_at: datetime
    scheduled_at: datetime
    zoom_link: str
    category: str


# Repo #
class EventRepository:
    def get_one(self, event_id: int) -> Optional[EventOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT event_id
                        , topic
                        , author
                        , partner
                        , paired
                        , expired
                        , created_at
                        , scheduled_at
                        , zoom_link
                        , category
                        FROM event
                        WHERE event_id = %s
                        """,
                        [event_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_event_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not find event."}

    def delete(self, event_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM event
                        WHERE event_id = %s
                        """,
                        [event_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def update(self, event_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE event
                        SET paired = true
                        WHERE event_id = %s
                        """,
                        [event_id],
                    )
                    return self.get_one(event_id)
        except Exception as e:
            return Error(
                message=f"Error occurred while updating 'event': {str(e)}"
            )

    def update_expired(self, event_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE event
                        SET expired = true
                        WHERE event_id = %s
                        """,
                        [event_id],
                    )
                    return self.get_one(event_id)
        except Exception as e:
            return Error(
                message=f"Error occurred while updating 'event': {str(e)}"
            )

    def get_all(self) -> Union[Error, List[EventOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT event_id
                        , topic
                        , author
                        , partner
                        , paired
                        , expired
                        , created_at
                        , scheduled_at
                        , zoom_link
                        , category
                        FROM event
                        ORDER BY scheduled_at;
                        """
                    )
                    return [self.record_to_event_out(record) for record in db]
        except Exception as e:
            return Error(
                message=f"Error occurred while retrieving 'events': {str(e)}"
            )

    def create(self, event: EventIn) -> EventOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO event
                            (
                                topic,
                                author,
                                partner,
                                paired,
                                expired,
                                created_at,
                                scheduled_at,
                                zoom_link,
                                category
                            )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING event_id;
                        """,
                        [
                            event.topic,
                            event.author,
                            event.partner,
                            event.paired,
                            event.expired,
                            event.created_at,
                            event.scheduled_at,
                            event.zoom_link,
                            event.category,
                        ],
                    )
                    event_id = result.fetchone()[0]
                    return self.event_in_to_out(event_id, event)
        except Exception as e:
            return Error(
                message=f"Error occurred while creating 'event': {str(e)}"
            )

    def event_in_to_out(self, event_id: int, event: EventIn):
        old_data = event.dict()
        return EventOut(event_id=event_id, **old_data)

    def record_to_event_out(self, record):
        return EventOut(
            event_id=record[0],
            topic=record[1],
            author=record[2],
            partner=record[3],
            paired=record[4],
            expired=record[5],
            created_at=record[6],
            scheduled_at=record[7],
            zoom_link=record[8],
            category=record[9],
        )

    def requested_event_out(self, record):
        return RequestedEventOut(
            event_id=record[0],
            topic=record[1],
            author=record[2],
            author_first_name=record[3],
            author_last_name=record[4],
            partner=record[5],
            paired=record[6],
            expired=record[7],
            created_at=record[8],
            scheduled_at=record[9],
            zoom_link=record[10],
        )

    def paired_event_out(self, record):
        return PairedEventOut(
            event_id=record[0],
            topic=record[1],
            author=record[2],
            author_first_name=record[3],
            author_last_name=record[4],
            partner=record[5],
            partner_first_name=record[6],
            partner_last_name=record[7],
            paired=record[8],
            expired=record[9],
            created_at=record[10],
            scheduled_at=record[11],
            zoom_link=record[12],
            category=record[13],
        )

    def get_paired_events_by_profile(
        self, profile_id: int
    ) -> List[PairedEventOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                          e.event_id,
                          e.topic,
                          e.author,
                          a.first_name AS author_first_name,
                          a.last_name AS author_last_name,
                          e.partner,
                          p.first_name AS partner_first_name,
                          p.last_name AS partner_last_name,
                          e.paired,
                          e.expired,
                          e.created_at,
                          e.scheduled_at,
                          e.zoom_link,
                          e.category
                        FROM event e
                        JOIN profile a ON e.author = a.profile_id
                        JOIN profile p ON e.partner = p.profile_id
                        WHERE (
                          author = %s
                          OR partner = %s
                          )
                        AND paired = %s
                        AND expired = %s
                        ORDER BY e.scheduled_at;
                        """,
                        [profile_id, profile_id, True, False],
                    )
                    # records = db.fetchall()
                    # pprint(records)
                    return [self.paired_event_out(record) for record in db]
        except Exception as e:
            return Error(
                message=f"Error occurred while retrieving 'events': {str(e)}"
            )

    def get_requested_events_by_profile(
        self, profile_id: int
    ) -> List[RequestedEventOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                          e.event_id,
                          e.topic,
                          e.author,
                          a.first_name AS author_first_name,
                          a.last_name AS author_last_name,
                          e.partner,
                          e.paired,
                          e.expired,
                          e.created_at,
                          e.scheduled_at,
                          e.zoom_link
                        FROM event e
                        JOIN profile a ON e.author = a.profile_id
                        JOIN profile p ON e.partner = p.profile_id
                        WHERE (
                          e.partner = %s
                          AND e.paired = %s
                          AND e.expired = %s
                          )
                        ORDER BY e.scheduled_at;
                        """,
                        [profile_id, False, False],
                    )
                    return [self.requested_event_out(record) for record in db]
        except Exception as e:
            print(f"Error occurred while retrieving 'events': {str(e)}")
            return []
