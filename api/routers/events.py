from fastapi import APIRouter, Depends, Response
from queries.events import (
    EventIn,
    EventOut,
    EventRepository,
    Error,
    RequestedEventOut,
    UpdateEventToPaired,
    UpdateEventToExpired,
    PairedEventOut,
)
from typing import Union, List, Optional

# Addon #
router = APIRouter()


# POST #
@router.post("/events", response_model=Union[EventOut, Error])
def create_event(
    event: EventIn, response: Response, repo: EventRepository = Depends()
):
    created_event = repo.create(event)
    if isinstance(created_event, EventOut):
        response.status_code = 201
    else:
        response.status_code = 400
    return created_event


@router.get("/events/{event_id}", response_model=Optional[EventOut])
def get_one_event(
    event_id: int,
    response: Response,
    repo: EventRepository = Depends(),
) -> EventOut:
    found_event = repo.get_one(event_id)
    if isinstance(found_event, EventOut):
        response.status_code = 201
    else:
        response.status_code = 404
    return found_event


# PUT #
@router.put("/events/{event_id}", response_model=Union[EventOut, Error])
def update_event(
    event_id: int,
    event: UpdateEventToPaired,  # might need to change this
    repo: EventRepository = Depends(),
) -> Union[EventOut, Error]:
    return repo.update(event_id)


@router.put(
    "/events_expired/{event_id}", response_model=Union[EventOut, Error]
)
def update_event_expired(
    event_id: int,
    event: UpdateEventToExpired,  # might need to change this
    repo: EventRepository = Depends(),
) -> Union[EventOut, Error]:
    return repo.update_expired(event_id)


# DELETE #
@router.delete("/events/{event_id}", response_model=bool)
def delete_event(
    event_id: int,
    repo: EventRepository = Depends(),
) -> bool:
    return repo.delete(event_id)


# GETS #
@router.get("/events", response_model=Union[Error, List[EventOut]])
def get_all(
    repo: EventRepository = Depends(),
):
    return repo.get_all()


@router.get(
    "/events_paired_by_profile/{profile_id}",
    response_model=List[PairedEventOut],
)
def get_paired_events_by_profile(
    profile_id: int, repo: EventRepository = Depends()
) -> List[PairedEventOut]:
    events = repo.get_paired_events_by_profile(profile_id)
    return events


@router.get(
    "/events_requested_by_partner/{profile_id}",
    response_model=List[RequestedEventOut],
)
def get_requested_events_by_profile(
    profile_id: int, repo: EventRepository = Depends()
) -> List[RequestedEventOut]:
    events = repo.get_requested_events_by_profile(profile_id)
    return events
