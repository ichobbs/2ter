from fastapi import APIRouter, Depends, Response
from typing import Union, List
from queries.messages import MessageIn, MessageRepository, MessageOut, Error

# Addon #
router = APIRouter()


# POST #
@router.post("/messages", response_model=Union[MessageOut, Error])
def create_message(
    message: MessageIn, response: Response, repo: MessageRepository = Depends()
):
    create_message = repo.create(message)
    if isinstance(create_message, MessageOut):
        response.status_code = 201
    else:
        response.status_code = 400
    return create_message


# GETS #
@router.get("/messages", response_model=Union[Error, List[MessageOut]])
def get_messages(repo: MessageRepository = Depends()):
    return repo.get_all()


@router.get("/messages/{message_id}", response_model=Union[MessageOut, Error])
def get_one_message(
    message_id: int,
    repo: MessageRepository = Depends(),
) -> MessageOut:
    return repo.get_one(message_id)


@router.get(
    "/messages/received-by-profile/{profile_id}",
    response_model=List[MessageOut],
)
def get_received_messages_by_profile(
    profile_id: int, repo: MessageRepository = Depends()
) -> List[MessageOut]:
    return repo.get_received_messages_by_profile(profile_id)


@router.get(
    "/messages/sent-by-profile/{profile_id}", response_model=List[MessageOut]
)
def get_sent_messages_by_profile(
    profile_id: int, repo: MessageRepository = Depends()
) -> List[MessageOut]:
    return repo.get_sent_messages_by_profile(profile_id)


@router.put("/messages/{message_id}", response_model=Union[MessageOut, Error])
def update_message(
    message_id: int,
    message: MessageIn,
    repo: MessageRepository = Depends(),
) -> Union[MessageOut, Error]:
    return repo.update(message_id, message)


# DELETE #
@router.delete("/messages/{message_id}", response_model=bool)
def delete_message(
    message_id: int,
    repo: MessageRepository = Depends(),
) -> bool:
    return repo.delete(message_id)
