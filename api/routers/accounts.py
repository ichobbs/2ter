from fastapi import APIRouter, Depends, HTTPException, status, Request
from models.accounts import (
    Account,
    AccountsOut,
    AccountOutPUT,
    AccountPUT,
    SignupIn,
)
from queries.accounts import (
    AccountsRepository,
    AccountToken,
    Error,
    AuthenticationException,
)
from queries.profiles import (
    ProfileRepository,
    ProfileIn,
)
from typing import List, Union
from authenticator import MyAuthenticator
import os

# Addons #
authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])
router = APIRouter()


# POST #
@router.post("/api/accounts")
def create_account(
    info: SignupIn,
    accounts: AccountsRepository = Depends(),
    profiles: ProfileRepository = Depends(),
) -> AccountsOut:
    hashed_password = authenticator.hash_password(info.password)
    ar = Account(
        account_id=info.account_id,
        email=info.email,
        username=info.username,
    )
    try:
        pk = accounts.create(ar, hashed_password)
        profiles.create(
            ProfileIn(
                **{
                    "account": pk,
                    "picture_url": "",
                    "first_name": info.first_name,
                    "last_name": info.last_name,
                    "skills": "",
                    "interests": "",
                    "bio": "",
                }
            )
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e),
        )
    return accounts.get_account_by_id(pk)


# GETS #
@router.get("/api/accounts", response_model=Union[Error, List[Account]])
def list_accounts(
    repo: AccountsRepository = Depends(),
):
    return repo.get_all()


@router.get("/api/accounts/{pk}", response_model=AccountsOut)
async def get_account_details(
    pk: int,
    accounts: AccountsRepository = Depends(),
) -> AccountsOut:
    try:
        account = accounts.get_account_by_id(pk)
    except AuthenticationException:
        return HTTPException(status.HTTP_401_UNAUTHORIZED)
    return account


@router.get(
    "/accountByUsername/{username}", response_model=Union[AccountsOut, Error]
)
def get_account_by_username(
    account_username: str,
    repo: AccountsRepository = Depends(),
) -> AccountsOut:
    return repo.get_account_by_username(account_username)


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: Account = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


# PUT #
@router.put(
    "/api/accounts/{account_id}", response_model=Union[AccountOutPUT, Error]
)
def update_account(
    account_id: int, accounts: AccountPUT, repo: AccountsRepository = Depends()
) -> Union[AccountOutPUT, Error]:
    hashed_password = authenticator.hash_password(accounts.password)
    return repo.update(account_id, accounts, hashed_password)


# DELETE #
@router.delete("/api/accounts/{pk}", response_model=bool)
def delete_account(
    pk: int,
    repo: AccountsRepository = Depends(),
) -> bool:
    return repo.delete(pk)
