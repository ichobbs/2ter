steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE event (
            event_id SERIAL PRIMARY KEY NOT NULL,
            topic VARCHAR(50) NOT NULL,
            author int REFERENCES profile(profile_id) ON DELETE CASCADE,
            partner int REFERENCES profile(profile_id) ON DELETE CASCADE,
            paired BOOLEAN NOT NULL DEFAULT FALSE,
            expired BOOLEAN NOT NULL DEFAULT FALSE,
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            scheduled_at TIMESTAMP,
            zoom_link VARCHAR(500),
            category VARCHAR(100)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE event;
        """
    ]
]
