steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE accounts (
            account_id SERIAL PRIMARY KEY NOT NULL,
            email VARCHAR(250) UNIQUE NOT NULL,
            username VARCHAR(250) UNIQUE NOT NULL,
            password VARCHAR(250) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE accounts;
        """,
    ]
]
