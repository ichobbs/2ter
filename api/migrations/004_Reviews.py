# Create the table
steps = [
    [
        # CREATE TABLE
        """
        CREATE TABLE review (
            review_id SERIAL PRIMARY KEY NOT NULL,
            author INT NOT NULL REFERENCES profile(profile_id) ON DELETE CASCADE,
            reviewee INT NOT NULL REFERENCES profile(profile_id) ON DELETE CASCADE,
            date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
            rating INT NOT NULL,
            review_text TEXT
        );
        """,
        # DROP TABLE
        """
        DROP TABLE review;
        """
    ]
]
