steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE profile (
            profile_id SERIAL PRIMARY KEY NOT NULL,
            account INT NOT NULL REFERENCES accounts(account_id) ON DELETE CASCADE,
            picture_url VARCHAR,
            first_name VARCHAR(50) NOT NULL,
            last_name VARCHAR(50) NOT NULL,
            skills VARCHAR (1000),
            interests VARCHAR (1000),
            bio TEXT
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE profile;
        """
    ]
]
