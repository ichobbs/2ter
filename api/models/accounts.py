from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class Account(BaseModel):
    account_id: int
    username: str
    email: str


class AccountsIn(Account):
    password: str


class SignupIn(AccountsIn):
    first_name: str
    last_name: str


class AccountsOut(Account):
    account_id: int
    hashed_password: str


class AccountToken(Token):
    account: AccountsOut


class Error(BaseModel):
    message: str


class AuthenticationException(Exception):
    pass


class AccountPUT(BaseModel):
    email: str
    username: str
    password: str


class AccountOutPUT(BaseModel):
    email: str
    username: str
    hashed_password: str
