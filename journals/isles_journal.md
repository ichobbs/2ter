# 8/15
working events router/query; added migration for createevent, created placcehodler files for event.py

# 8/16
events entity complete: create, get all, get one, update, delete; functional getall events route/query

# 8/21
users.py functional in queries + routers, added to main.py; added users.py routers

# 8/22
reviews create try/exccept handling added; foreignkey constraints/pointers added for profiles -> accounts, reviews/event/messages -> profiles; working accounts integration

# 8/23
brinnign dev branch into alignment w/ main... renamed migration filesl; removed users.dm files, as this was duplicate code ref to profiles that felix made to facilitate the effective implementation of auth; changed account_id to account for key in profiels table, IAW standardization. modified tables to include ON CASCADE DELETE; fixed 'delete account' bug.

# 8/24
renamed currentprofile to myprofile to avoid confusion, over against viewprofile; refactored away from profileid to profiledict on several occasions

# 8/25
created uselocalstorage, fixed the myprofile reviewslist such that it does not stick to previously viewed profile. removed header/footer from nav.jsx, flipped email and username in query to fix wrong-reference bug; restructured routes to put accounts at the top of docs; added getreviewsbyprofile query, working reviews card

# 8/28
profile cards work; added account_id to signupform for profile fetch purposes

# 8/29
stopped chasing tokens

# 8/30
fixed messages fetch to get on messages.jsx, commented out setprofileID and related; my profile no lands on profile associated with sign-in, trying to get enough refreshes for getusernamebyprofile to do its work; search account by username works

# 8/31
working event queries, no front-end tho just thru docs; signup now works to log in and assign myProfile; create account now creates profiles concurrently

# 9/1
troubleshooting events sticking with initial load; events list works, bu tnot with join; refresh on viewed profile redirects to profiles... fixed myprofile getting overwritten by viewprofile in localstorage by NOT storing viewprofile in localstorage.


# 9/5
toyed with npm install, broke things badly. myprofile requested appointments now persist through refresh; view profile appointments work as intended

# 9/6
posting event now works; lots of trouble-shooting for mui css implementation that didn't quite work. posting reviews now works.

# 9/7
added search function to profiles.jsx for 'skills' and 'interests' with toggle between user filter choice. filter for 'paired' events now works, to effect 'schedule' card effective display of events; working 'put' function for toggling 'paired' boolean to 'true' for query filter


# 9/11
minor touch-up, chasing some bugs... minor presentation to Candice today revealed missing conditional display feature for one button. trying to figure out deployment; freddy and ian are taking point. too many cooks in the kitchen

# 9/8
group focus on merging and debugging main. toyed with CSS, determined that there's not really enough time to revamp things there. shifted attention to auth. added effective 'cancel' button on requested appointments. flake8 fixed api; passing flake8 test for deployment finished 'message' capability from requested appointments; added dynamic information to message modal from requested appointments. intercepted default behavior on 'enter' key for search bar in 'profiles.jsx' to cause enter to run 'onClick' function
