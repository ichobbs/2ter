function bubbleSort(items) {
  const length = items.length;
  for (let i = 0; i < length; i++) {
    for (let j = 0; j < length - (i + 1); j++) {
      if (items[j] > items[j + 1]) {
        [items[j], items[j + 1]] = [items[j + 1], items[j]];
        // let temp = items[j]
        // items[j] = items[j + 1]
        // items[j+1] = temp
      }
    }
  }
  return items;
}

// console.log(bubbleSort([45, 13, 1, 5]));

function selectionSort(items) {
  const length = items.length;
  for (let start = 0; start < length; start++) {
    let min_idx = start;
    let min_val = items[start];
    for (let j = start + 1; j < length; j++) {
      if (items[j] < min_val) {
        min_idx = j;
        min_val = items[j];
      }
    }
    if (min_idx > start) {
      [items[start], items[min_idx]] = [items[min_idx], items[start]];
    }
  }
  return items;
}

// console.log(selectionSort([13, 22, 4, 9]));

// const input = [4, 3, 2, 1]

function print_number(numbers) {
  for (let num of numbers) {
    console.log(num);
  }
  return "the 'of' for-loop works to print the values of an interable";
}

// console.log(print_number(input))

function print_number2(numbers) {
  for (let index in numbers) {
    console.log(index);
  }
  return "the 'in' for-loop works to print the indexes of an iterable";
}

// console.log(print_number2(input));

function mergeSort(values, left = null, right = null) {
  if (left === null && right === null) {
    left = 0;
    right = values.length - 1;
  }

  if (left >= right) {
    return;
  }

  let middle = Math.floor((right + left) / 2);
  mergeSort(values, left, middle);
  mergeSort(values, middle + 1, right);
  merge(values, left, middle, right);
  return values;
}

function merge(values, left, middle, right) {
  let rightStart = middle + 1;
  if (values[middle] <= values[rightStart]) {
    return;
  }

  while (left <= middle && rightStart <= right) {
    if (values[left] <= values[rightStart]) {
      left += 1;
    } else {
      let value = values[rightStart];
      let index = rightStart;
      while (index != left) {
        values[index] = values[index - 1];
        index -= 1;
      }
      values[left] = value;
      left += 1;
      middle += 1;
      rightStart += 1;
    }
  }
}

const input = [45, 13, 1, 5];
console.log(mergeSort(input));
