## 8/16

- first journal entry added, did not know the group needed these files
  in this directory

- As of now, endpoints for profile and profile lists views have been started.
  while the endpoints give back 200 responses, i placed try/except blocks in the code to catch any errors. as of now i cannot get the responses to give back the data entries and are only given back the errors i wrote in.

- Really need to figure out this one backend point to be able to test
  how front end relations work.

## 9/11

- between the first entry and this one ive made many working branchs and pruned them to work on features. somehwere along the way, every journal entry i wrote has been lost in the many merges and pruning. i chock that up to the lack of pushing enough commits from my working branchs.

- that being said the project has taken alot of time and work to get to a MVP that works correctly. so far i think we have enough working endpoints to get the project to deploy and pass the minimum requirements to pass. i pray that this work isnt in vain, there has been many conflicts of interest and approaches among the group and many long, tedious merge sessions. im confident this isnt going to have an adverse effect on our grade going further, im happy witht he work we did, and the progress our group has made socially to come together on a shared goal.
