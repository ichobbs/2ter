def roman_numeral(num):
    newdict = {
        1: "I",
        2: "II",
        3: "III",
        4: "IV",
        5: "V",
        6: "VI",
        7: "VII",
        8: "VIII",
        9: "IX",
        10: "X",
    }
    return newdict[num]


def int_to_roman(n):
    val = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    syb = [
        "M",
        "CM",
        "D",
        "CD",
        "C",
        "XC",
        "L",
        "XL",
        "X",
        "IX",
        "V",
        "IV",
        "I",
    ]
    roman_num = ""
    i = 0
    while n > 0:
        for _ in range(n // val[i]):
            roman_num += syb[i]
            n -= val[i]
        i += 1
    return roman_num


def find_larger_area(s, r):
    import math
    square = s * s
    circle = math.pi * r ** 2
    if square > circle:
        return "SQUARE"
    return "CIRCLE"


# def does_product_exist(nums, left, right, target):
#     newlist = nums[left:right+1]
#     for i in range(len(newlist)):
#         for j in range(i + 1, len(newlist)):
#             if newlist[i] * newlist[j] == target:
#                 return True
#     return False


def does_product_exist(nums, left, right, target):
    seen = set()
    newlist = nums[left:right+1]

    for num in newlist:
        if target % num == 0 and target // num in seen:
            return True
        seen.add(num)

    return False


    print(does_product_exist([2, 4, 2, 4, 2, 4], 0, 3, 16))


def make_integer(nums, length):
    nums.sort()
    newlist = nums[:length]
    # newlist2 = [str(num) for num in newlist]
    # newstring = ""
    # for num in newlist2:
    #     newstring += num
    # return int(newstring)
    return int(''.join(str(digit) for digit in newlist))


def merge_sort(values, left=None, right=None):
    if left is None and right is None:
        left = 0
        right = len(values) - 1

    if left >= right:
        return

    middle = (right + left) // 2

    merge_sort(values, left, middle)

    merge_sort(values, middle + 1, right)

    merge(values, left, middle, right)


def merge(values, left, middle, right):
    right_start = middle + 1

    if values[middle] <= values[right_start]:
        return

    while left <= middle and right_start <= right:
        if values[left] <= values[right_start]:
            left += 1
        else:
            value = values[right_start]
            index = right_start

            while index != left:
                values[index] = values[index - 1]
                index -= 1

            values[left] = value

            left += 1
            middle += 1
            right_start += 1


def find_list_difference(nums1, nums2):
    return sorted([num for num in nums1 if num not in nums2])


    print(find_list_difference([5, 2, 4, 3, 1], [4, 2]))


def intersection(nums):
    common = set.intersection(*map(set, nums))
    newlist = [number for number in common]
    return newlist


    input = [[3, 1, 2, 4, 5], [1, 2, 3, 4], [3, 4, 5, 6]]
    print(intersection(input))


    num_strings = ["1", "2", "3", "4", "5"]
    num_integers = list(map(int, num_strings))
    print(num_integers)


def join(pipes, step):
    pipes[step[0]-1] += pipes[step[0]]
    pipes.pop(step[0])
    return pipes

def split(pipes, step):
    pipes.insert(step[0], pipes[step[0]-1] - step[1])
    pipes[step[0]-1] = step[1]
    return pipes

def pipe_outputs(num_pipes, steps):
    pipes = [8]*num_pipes
    for step in steps:
        if len(step) == 1:
            join(pipes, step)
        else:
            split(pipes, step)
    return pipes


    print(pipe_outputs(3, [[2, 4], [1], [1], [1, 2]]))

