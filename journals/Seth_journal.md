8/30 Wednesday:

Working on creating a ReviewsForm component to implement functionality for other users to leave a review for other users profiles.
Hoping to implement conditional input buttons according to logged in and authenticated status to avoid a disorganized user experience. For example, a profile owner should not be able to leave a review for themselves, and other users who are not logged in and authenticated should not be able to leave reviews.

I'd like to also include functionality that averages all submitted reviews into an average score.

9/01 Friday:

Working on implementing conditional rendering for clickable buttons on other parts of the website. Learning some more about conditional rendering through token authentication using the jwtdown authentication system. Also, learning conditional rendering by comparing the logged in user's profile id and the profile that is currently being viewed to separate functionality according to which user is which.
