import React, { useState, useEffect } from "react";
import { Typography, TextField, Button, Grid, Box, Modal } from "@mui/material";
import { useContextStore } from "./ContextStore";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

function ProfileSkills({ initialSkills }) {
  let [editedSkills, setEditedSkills] = useState(initialSkills);
  const [setIsEditing] = useState(false);
  const [open, setOpen] = useState(false);

  const { myProfileDict, viewProfileDict, request, urls, setMyProfileDict } =
    useContextStore();

  const { token } = useAuthContext();

  const putToProfile = async (profileData) => {
    try {
      await request.put(
        urls.profile(myProfileDict.profile_id),
        profileData,
        setMyProfileDict(profileData)
      );
    } catch (error) {
      console.error("Error in putToProfile:", error);
    }
  };

  const handleEditButtonClick = () => {
    setOpen(true);
    setIsEditing(true);
  };

  const handleSaveButtonClick = async () => {
    const existingProfileData = { ...myProfileDict };
    setEditedSkills(editedSkills);
    setIsEditing(false);
    setOpen(false);
    existingProfileData.skills = editedSkills;
    await putToProfile(existingProfileData);
  };

  const handleCancelButtonClick = () => {
    setEditedSkills(initialSkills);
    setIsEditing(false);
    setOpen(false);
  };

  if (editedSkills == undefined) {
    editedSkills = initialSkills;
  }

  const isOwner = myProfileDict.profile_id === viewProfileDict.profile_id;
  const isAuthenticated = !!token;

  return (
    <Grid
      item
      xl={12}
      lg={12}
      s={12}
      xs={12}
      sx={{ border: "1px solid black" }}
    >
      <Typography align="center" variant="h6">
        Skills
      </Typography>
      <Box>
        <Modal open={open} onClose={() => setOpen(false)}>
          <Box
            width="50%"
            p={3}
            sx={{
              backgroundColor: "primary.dark",
              zIndex: 1600,
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          >
            <TextField
              fullWidth
              multiline
              variant="outlined"
              value={editedSkills}
              onChange={(event) => setEditedSkills(event.target.value)}
              sx={{ marginBottom: 2 }}
            />
            <Box display="flex" justifyContent="center">
              <Button
                variant="contained"
                color="secondary"
                onClick={handleSaveButtonClick}
                sx={{ margin: 1 }}
              >
                Save
              </Button>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleCancelButtonClick}
                sx={{ margin: 1 }}
              >
                Cancel
              </Button>
            </Box>
          </Box>
        </Modal>
      </Box>
      <Box>
        <Typography
          align="center"
          variant="body1"
          sx={{ whiteSpace: "pre-line" }}
        >
          {editedSkills}
        </Typography>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-end",
            borderTop: "1px solid black",
          }}
        >
          {isOwner && isAuthenticated && (
            <Button
              size="small"
              variant="contained"
              color="secondary"
              onClick={handleEditButtonClick}
              sx={{ margin: 1, fontSize: "0.7rem" }}
            >
              Edit
            </Button>
          )}
        </Box>
      </Box>
    </Grid>
  );
}

export default ProfileSkills;
