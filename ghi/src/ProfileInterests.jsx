import React, { useState } from "react";
import { Typography, TextField, Button, Grid, Box, Modal } from "@mui/material";
import { useContextStore } from "./ContextStore";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

function ProfileInterests({ initialInterests }) {
  const { token } = useAuthContext();
  let [editedInterests, setEditedInterests] = useState(initialInterests);
  const [isEditing, setIsEditing] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);

  const { myProfileDict, request, urls, setMyProfileDict, viewProfileDict } =
    useContextStore();

  const putToProfile = async (profileData) => {
    try {
      await request.put(
        urls.profile(myProfileDict.profile_id),
        profileData,
        setMyProfileDict(profileData)
      );
    } catch (error) {
      console.error("Error in putToProfile:", error);
    }
  };

  const handleEditButtonClick = () => {
    setIsEditing(true);
    setModalOpen(true);
  };

  const handleSaveButtonClick = async () => {
    const existingProfileData = { ...myProfileDict };
    setEditedInterests(editedInterests);
    setIsEditing(false);
    setModalOpen(false);

    existingProfileData.interests = editedInterests;
    await putToProfile(existingProfileData);
  };

  const handleCancelButtonClick = () => {
    setEditedInterests(initialInterests);
    setIsEditing(false);
    setModalOpen(false);
  };

  if (editedInterests == undefined) {
    editedInterests = initialInterests;
  }

  const isOwner = myProfileDict.profile_id === viewProfileDict.profile_id;
  const isAuthenticated = !!token;

  return (
    <Grid
      item
      xl={12}
      lg={12}
      s={12}
      xs={12}
      sx={{ border: "1px solid black" }}
    >
      <Typography align="center" variant="h6">
        Interests
      </Typography>
      <Box>
        <Modal open={modalOpen} onClose={() => setModalOpen(false)}>
          <Box
            width="50%"
            p={3}
            sx={{
              backgroundColor: "primary.dark",
              zIndex: 1600,
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          >
            <TextField
              fullWidth
              multiline
              variant="outlined"
              value={editedInterests}
              onChange={(event) => setEditedInterests(event.target.value)}
              sx={{ marginBottom: 2 }}
            />
            <Box display="flex" justifyContent="center">
              {isOwner && isEditing && (
                <>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={handleSaveButtonClick}
                    sx={{ margin: 1 }}
                  >
                    Save
                  </Button>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={handleCancelButtonClick}
                    sx={{ margin: 1 }}
                  >
                    Cancel
                  </Button>
                </>
              )}
            </Box>
          </Box>
        </Modal>
      </Box>
      <Box>
        <Typography
          align="center"
          variant="body1"
          sx={{ whiteSpace: "pre-line" }}
        >
          {editedInterests}
        </Typography>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-end",
            borderTop: "1px solid black",
          }}
        >
          {isOwner && !isEditing && isAuthenticated && (
            <Button
              size="small"
              variant="contained"
              color="secondary"
              onClick={handleEditButtonClick}
              sx={{ margin: 1, fontSize: "0.7rem" }}
            >
              Edit
            </Button>
          )}
        </Box>
      </Box>
    </Grid>
  );
}

export default ProfileInterests;
