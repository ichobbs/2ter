import React, { useEffect, useState } from "react";
import { motion } from "framer-motion";
import {
  Box,
  Typography,
  List,
  ListItemText,
  ListItem,
  ListSubheader,
  Button,
  Modal,
  TextField,
  Switch,
  Container,
} from "@mui/material";
import { useContextStore } from "./ContextStore";

function Messages() {
  const {
    getMessages,
    urls,
    request,
    getReceivedMessages,
    receivedMessages,
    getSentMessages,
    sentMessages,
    profilesDict,
    getProfiles
  } = useContextStore();
  const [expandedMessageId, setExpandedMessageId] = useState(null);
  const [open, setModalOpen] = useState(false);
  const [replyBody, setReplyBody] = useState();
  const [replySubject, setReplySubject] = useState();
  const [originalMessage, setOriginalMessage] = useState();
  const [viewedMessages, setViewedMessages] = useState();
  const label = { inputProps: { "aria-label": "messages toggle" } };

  const postNewMessage = async (messageData) => {
    getReceivedMessages();
    getSentMessages();
    try {
      await request.post(urls.messages, messageData, getMessages);
      getMessages();
    } catch (error) {
      console.error("Error in putToProfile:", error);
    }
  };

  const handleMessageToggle = () => {
    if (viewedMessages == receivedMessages) {
      setViewedMessages(sentMessages);
    } else {
      setViewedMessages(receivedMessages);
    }
  };

  const handleToggleExpand = (messageId) => {
    if (expandedMessageId === messageId) {
      setExpandedMessageId(null);
    } else {
      setExpandedMessageId(messageId);
    }
  };

  const handleReplyClick = (originalMessageId) => {
    setOriginalMessage(
      viewedMessages.find((message) => message.message_id === originalMessageId)
    );
    setModalOpen(true);
  };

  const handleReplyCancel = () => {
    setModalOpen(false);
    setReplyBody("");
  };

  const handleReplySend = () => {

    if (viewedMessages === receivedMessages) {
      const messageSender = originalMessage.receiver;
      const messageReceiver = originalMessage.sender;
      const messageData = {
        sender: messageSender,
        receiver: messageReceiver,
        date: Date.now(),
        subject: replySubject,
        body: replyBody,
      };

      postNewMessage(messageData);

      setModalOpen(false);
      setReplyBody("");
    } else {
      const messageSender = originalMessage.sender;
      const messageReceiver = originalMessage.receiver;
      const messageData = {
        sender: messageSender,
        receiver: messageReceiver,
        date: Date.now(),
        subject: replySubject,
        body: replyBody,
      };

      postNewMessage(messageData);
      setModalOpen(false);
      setReplyBody("");
    }
  };

  useEffect(() => {
    getSentMessages();
    getReceivedMessages();
    getProfiles();
  }, []);

  useEffect(() => {
    if (viewedMessages != receivedMessages && viewedMessages != sentMessages) {
      setViewedMessages(receivedMessages);
    }
  }, [receivedMessages]);

  useEffect(() => {
    if (originalMessage != undefined) {
      setReplySubject(`Re: ${originalMessage.subject}`);
    }
  }, [originalMessage]);

  return (
    <Container>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
      <Box
        sx={{
          width: "100%",
          margin: "auto",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "primary.dark",
          border: "1px solid black",
        }}
      >
        <List
          aria-labelledby="inbox-title"
          sx={{ width: "95%", backgroundColor: "primary.main" }}
          subheader={
            <ListSubheader
              component="div"
              id="inbox-title"
              variant="h1"
              sx={{ width: "100%", backgroundColor: "primary.dark", fontSize: 30, color: "black" }}
            >
              Inbox
              <Box sx={{fontSize: 20}}>
                Received Messages
                <Switch
                  {...label}
                  color="default"
                  onChange={handleMessageToggle}
                />
                Sent Messages
              </Box>
            </ListSubheader>
          }
        >
          {viewedMessages?.map((message) => (
            <ListItem
              key={message.message_id}
              sx={{ width: "95%", backgroundColor: "primary.main" }}
            >
              <ListItemText
                primary={
                    (() => {
                      let profile;
                      if (viewedMessages === sentMessages) {
                        profile = profilesDict.find(profile => profile.profile_id === message.receiver);
                      } else {
                        profile = profilesDict.find(profile => profile.profile_id === message.sender);
                      }
                      
                      if (profile) {
                        return `${profile.first_name} ${profile.last_name || ''} || ${message.subject}`;
                      } else {
                        return "Unknown";
                      }
                    })()
                  }
                secondary={
                  <React.Fragment>
                    <Typography component="span">
                      {" "}
                      {/* Use Typography component here */}
                      <Typography
                        component="span"
                        variant="body2"
                        sx={{
                          display: "block",
                          maxWidth: 800,
                          whiteSpace:
                            expandedMessageId === message.message_id
                              ? "normal"
                              : "nowrap",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                        }}
                      >
                        {message.body}
                      </Typography>
                    </Typography>
                    <Typography variant="caption" color="textSecondary">
                      {new Date(message.date).toLocaleString()}
                    </Typography>
                    <Button
                      size="small"
                      variant="contained"
                      color="secondary"
                      onClick={() => handleToggleExpand(message.message_id)}
                      sx={{ margin: 1, fontSize: "0.7rem" }}
                    >
                      {expandedMessageId === message.message_id
                        ? "Collapse"
                        : "Expand"}
                    </Button>
                    <Button
                      size="small"
                      variant="contained"
                      color="secondary"
                      onClick={() => handleReplyClick(message.message_id)}
                      sx={{ margin: 1, fontSize: "0.7rem" }}
                    >
                      {receivedMessages === viewedMessages
                        ? "Reply"
                        : "Send New Message"}
                    </Button>
                  </React.Fragment>
                }
              />
            </ListItem>
          ))}
        </List>
        <Modal
          open={open}
          onClose={() => setModalOpen(false)}
          aria-labelledby="reply-modal-title"
        >
          <Box
            width="80%"
            p={3}
            sx={{
              backgroundColor: "primary.dark",
              zIndex: 1600,
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          >
            Subject
            <TextField
              fullWidth
              multiline
              variant="outlined"
              value={replySubject}
              onChange={(event) => setReplySubject(event.target.value)}
              sx={{ marginBottom: 2 }}
            />
            Message
            <TextField
              fullWidth
              multiline
              variant="outlined"
              value={replyBody}
              onChange={(event) => setReplyBody(event.target.value)}
              sx={{ marginBottom: 2 }}
            />
            <Box display="flex" justifyContent="center">
              <Button
                variant="contained"
                color="secondary"
                onClick={handleReplySend}
                sx={{ margin: 1 }}
              >
                Send
              </Button>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleReplyCancel}
                sx={{ margin: 1 }}
              >
                Cancel
              </Button>
            </Box>
          </Box>
        </Modal>
      </Box>
      </motion.div>
    </Container>
  );
}

export default Messages;
