import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useContextStore } from "./ContextStore";
import { styled } from "@mui/material/styles";
import {
  Card,
  CardHeader,
  Box,
  Grid,
} from "@mui/material";
import ProfileInterests from "./ProfileInterests";
import ProfileSkills from "./ProfileSkills";
import ProfileAbout from "./ProfileAbout";

function Bio() {
  const { myProfileDict, viewProfileDict } = useContextStore();
  const location = useLocation();

  const profileDict =
    location.pathname === "/my_profile" ? myProfileDict : viewProfileDict;

  return (
    <Box height="400px"
      sx={{
        p: 2,
        border: "1px solid black",
        backgroundColor: "primary.dark",
      }}>
      <Card
        sx={{
          p: 2,
          border: "1px solid black",
          backgroundColor: "primary.main",
          height: "100%",
          maxHeight: 400,
        }}
      >
        <CardHeader title="About" />
        <Grid container direction="row" justifyContent="center">
          <Grid item xl={4} lg={4} s={4} xs={12} sx={{ margin: 1 }}>
            {/* Skills */}
            <Box sx={{ marginBottom: 1 }}>
              <ProfileSkills initialSkills={profileDict?.skills} />
            </Box>
            {/* Interests */}
            <Box>
              <ProfileInterests initialInterests={profileDict?.interests} />
            </Box>
          </Grid>
          <Grid item xl={7} lg={7} s={7} xs={12}>
            <Box sx={{ margin: 1 }}>
              <ProfileAbout initialAbout={profileDict?.bio} />
            </Box>
          </Grid>
        </Grid>
      </Card>
    </Box>
  );
}

export default Bio;
