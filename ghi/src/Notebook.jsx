import React, { useState, useEffect } from "react";
import { useContextStore } from "./ContextStore";
import {
  Card,
  CardHeader,
  Box,
} from "@mui/material";

function Notebook() {
  return (
    <Box height="400px"
      sx={{
        p: 2,
        border: "1px solid black",
        backgroundColor: "primary.dark",
      }}>
      <Card
        sx={{
          p: 2,
          border: "1px solid black",
          backgroundColor: "primary.main",
          height: "100%",
        }}
      >
        <CardHeader title="Notebook" sx={{ textAlign: "center" }} />
      </Card>
    </Box>
  );
}

export default Notebook;
