import React from 'react';
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import ReactDOM from 'react-dom/client';
import App from './App';
import ContextProvider from './ContextStore';
import CssBaseline from '@mui/material/CssBaseline';
import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  palette: {
    primary: {
        light: '#3ed666',
        main: '#33a322',
        dark: '#227a39'
    },
    secondary: {
        light: '#ffd54f',
        main: '#ffca28',
        dark: '#ffa000'
    },
  },
  spacing: 6
});

function Main() {
  // const { token } = useAuthContext();

  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold text-body">2ter</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">Welcome to 2ter, a great place to meet and learn</p>
      </div>
    </div>
  );
}
export default Main;

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ContextProvider>
      <AuthProvider>
        <CssBaseline />
        <App />
      </AuthProvider>
    </ContextProvider>
  </React.StrictMode>
);
