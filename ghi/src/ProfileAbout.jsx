import React, { useState } from "react";
import { Typography, TextField, Button, Grid, Box, Modal } from "@mui/material";
import { useContextStore } from "./ContextStore";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

function ProfileAbout({ initialAbout }) {
  const { token } = useAuthContext();
  let [editedAbout, setEditedAbout] = useState(initialAbout);
  const [setIsEditing] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);

  const { myProfileDict, request, urls, setMyProfileDict, viewProfileDict } =
    useContextStore();

  const putToProfile = async (profileData) => {
    try {
      await request.put(
        urls.profile(myProfileDict.profile_id),
        profileData,
        setMyProfileDict(profileData)
      );
    } catch (error) {
      console.error("Error in putToProfile:", error);
    }
  };

  const handleEditButtonClick = () => {
    setIsEditing(true);
    setModalOpen(true);
  };

  const handleSaveButtonClick = async () => {
    const existingProfileData = { ...myProfileDict };
    setEditedAbout(editedAbout);
    setIsEditing(false);
    setModalOpen(false);

    existingProfileData.bio = editedAbout;
    await putToProfile(existingProfileData);
  };

  const handleCancelButtonClick = () => {
    setEditedAbout(initialAbout);
    setIsEditing(false);
    setModalOpen(false);
  };

  if (editedAbout == undefined) {
    editedAbout = initialAbout;
  }

  const isOwner = myProfileDict.profile_id === viewProfileDict.profile_id;
  const isAuthenticated = !!token;

  return (
    <Grid
      item
      xl={12}
      lg={12}
      s={12}
      xs={12}
      sx={{ border: "1px solid black" }}
    >
      <Typography align="center" variant="h6">
        Bio
      </Typography>
      <Box>
        <Modal open={modalOpen} onClose={() => setModalOpen(false)}>
          <Box
            width="80%"
            p={3}
            sx={{
              backgroundColor: "primary.dark",
              zIndex: 1600,
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          >
            <TextField
              fullWidth
              multiline
              variant="outlined"
              value={editedAbout}
              onChange={(event) => setEditedAbout(event.target.value)}
              sx={{ marginBottom: 2 }}
            />
            <Box display="flex" justifyContent="center">
              <Button
                variant="contained"
                color="secondary"
                onClick={handleSaveButtonClick}
                sx={{ margin: 1 }}
              >
                Save
              </Button>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleCancelButtonClick}
                sx={{ margin: 1 }}
              >
                Cancel
              </Button>
            </Box>
          </Box>
        </Modal>
      </Box>
      <Box>
        <Box sx={{ display: "flex", flexDirection: "column" }}>
          <Typography
            align="left"
            variant="body1"
            sx={{ whiteSpace: "pre-line", overflow: "auto", maxHeight: 200 }}
          >
            {editedAbout}
          </Typography>
          <Box
            sx={{
              backgroundColor: "black",
              height: 1,
              width: "100%",
              marginTop: 1,
              borderTop: "1px solid black",
            }}
          />
          {isOwner && isAuthenticated && (
            <Button
              size="small"
              variant="contained"
              color="secondary"
              onClick={handleEditButtonClick}
              sx={{
                maxWidth: 10,
                margin: "1px auto", // Center the button horizontally
                fontSize: "0.7rem",
                marginLeft: "auto", // Align the button to the right
              }}
            >
              Edit
            </Button>
          )}
        </Box>
      </Box>
    </Grid>
  );
}

export default ProfileAbout;
