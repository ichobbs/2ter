/* eslint-disable react-refresh/only-export-components */
/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
import React, { useState, createContext, useContext } from "react";
import useLocalStorage from "./useLocalStorage";

export const ContextStore = createContext(null);
const baseUrl = process.env.REACT_APP_API_HOST;

export default ({ children }) => {
  const urls = {
    profiles: `${baseUrl}/profiles`,
    profile: (profile_id) => `${baseUrl}/profiles/${profile_id}`,
    events: `${baseUrl}/events`,
    event: (event_id) => `${baseUrl}/events/${event_id}`,
    eventExpire: (event_id) => `${baseUrl}/events_expired/${event_id}`,
    messages: `${baseUrl}/messages`,
    message: (message_id) => `${baseUrl}/messages/${message_id}`,
    receivedMessagesUrl: (profile_id) =>
      `${baseUrl}/messages/received-by-profile/${profile_id}`,
    sentMessagesUrl: (profile_id) =>
      `${baseUrl}/messages/sent-by-profile/${profile_id}`,
    reviews: `${baseUrl}/reviews`,
    filteredReviews: (reviewee) => `${baseUrl}/reviews_by_profile/${reviewee}`,
    review: (review_id) => `${baseUrl}/reviews/${review_id}`,
    accounts: `${baseUrl}/api/accounts`,
    account: (pk) => `${baseUrl}/api/accounts/${pk}`,
    accountByUsername: (username) =>
      `${baseUrl}/accountByUsername/%7Busername%7D?account_username=${username}`,
    profileByAccount: (account_id) =>
      `${baseUrl}/profileByAccountId/${account_id}`,
    eventsPairedByProfile: (profile_id) =>
      `${baseUrl}/events_paired_by_profile/${profile_id}`,
    eventsRequestedByPartner: (profile_id) =>
      `${baseUrl}/events_requested_by_partner/${profile_id}`,
  };

  const request = {
    get: async (url, callback, key = null) => {
      const response = await fetch(url);
      if (response.ok) {
        let data = await response.json();
        if (key) {
          data = data[key];
        }
        const value = callback(data);
        return value;
      } else {
        console.log(response);
      }
    },
    post: async (url, data, callback) => {
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        callback();
      } else {
        console.log(response);
      }
    },
    put: async (url, data, callback) => {
      const fetchConfig = {
        method: "put",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok && callback instanceof Function) {
        callback();
      } else {
        console.log(response);
      }
    },
    delete: async (url, callback) => {
      const fetchConfig = {
        method: "delete",
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        callback();
      } else {
        console.log(response);
      }
    },
  };

  const [profilesDict, setProfileDict] = useState([]);
  const [viewProfileId, setViewProfileId] = useState("");
  const [viewProfileDict, setViewProfileDict] = useState({});
  const [myAccount, setMyAccount] = useLocalStorage({});
  const [myProfileDict, setMyProfileDict] = useLocalStorage({});
  const [messagesDict, setMessagesDict] = useState([]);
  const [reviewee, setReviewee] = useState();
  const [reviewsByReviewee, setReviewsByReviewee] = useState([]);
  const [signupProfile, setSignupProfile] = useState({});
  const [pairedEvents, setPairedEvents] = useState([]);
  const [requestedEvents, setRequestedEvents] = useState([]);
  const [receivedMessages, setReceivedMessages] = useState([]);
  const [sentMessages, setSentMessages] = useState([]);

  const getViewedProfilePairedEvents = async () =>
    await request.get(
      urls.eventsPairedByProfile(viewProfileId),
      setPairedEvents
    );
  const getMyProfilePairedEvents = async () =>
    await request.get(
      urls.eventsPairedByProfile(myProfileDict.profile_id),
      setPairedEvents
    );
  const getViewProfileRequestedEvents = async () =>
    await request.get(
      urls.eventsRequestedByPartner(viewProfileId),
      setRequestedEvents
    );
  const getMyProfileRequestedEvents = async () =>
    await request.get(
      urls.eventsRequestedByPartner(myProfileDict.profile_id),
      setRequestedEvents
    );

  const getProfiles = async () =>
    await request.get(urls.profiles, setProfileDict);

  const getViewedProfileDict = async () =>
    await request.get(urls.profile(viewProfileId), setViewProfileDict);

  const getMyProfileDict = async (account) => {
    await request.get(urls.profileByAccount(account.account_id), (data) => {
      setMyProfileDict(data);
    });
  };

  const getMessages = async () =>
    await request.get(urls.messages, setMessagesDict);

  const getReviewsByReviewee = async () =>
    await request.get(urls.filteredReviews(reviewee), setReviewsByReviewee);

  const getAccountByLoginUsername = async (username) => {
    return await request.get(urls.accountByUsername(username), (data) => {
      setMyAccount(data);
      return data;
    });
  };

  const getReceivedMessages = async () =>
    await request.get(
      urls.receivedMessagesUrl(myProfileDict.profile_id),
      setReceivedMessages
    );

  const getSentMessages = async () =>
    await request.get(
      urls.sentMessagesUrl(myProfileDict.profile_id),
      setSentMessages
    );

  const postProfileBySignup = async () =>
    await request.post(urls.profiles(signupProfile));

  function formatDate(isoDate) {
    // used to format date
    const options = {
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
    }; // to m/d/y format
    return new Date(isoDate).toLocaleDateString(undefined, options); // using .toLocaleDateString
  }

  const store = {
    urls: urls,
    request: request,
    profilesDict: profilesDict,
    messagesDict: messagesDict,
    reviewsByReviewee: reviewsByReviewee,
    reviewee: reviewee,
    viewProfileDict: viewProfileDict,
    pairedEvents: pairedEvents,
    myAccount: myAccount,
    myProfileDict: myProfileDict,
    requestedEvents: requestedEvents,
    receivedMessages: receivedMessages,
    sentMessages: sentMessages,

    getProfiles: getProfiles,
    getMessages: getMessages,
    getReviewsByReviewee: getReviewsByReviewee,
    getViewedProfileDict: getViewedProfileDict,
    getViewedProfilePairedEvents: getViewedProfilePairedEvents,
    getAccountByLoginUsername: getAccountByLoginUsername,
    getMyProfileDict: getMyProfileDict,
    getMyProfilePairedEvents: getMyProfilePairedEvents,
    getViewProfileRequestedEvents: getViewProfileRequestedEvents,
    getMyProfileRequestedEvents: getMyProfileRequestedEvents,
    getReceivedMessages: getReceivedMessages,
    getSentMessages: getSentMessages,
    //
    setReviewee: setReviewee,
    setReviewsByReviewee: setReviewsByReviewee,
    setMyProfileDict: setMyProfileDict, // blue because useLocalStorage
    setSignupProfile: setSignupProfile,
    setViewProfileId: setViewProfileId,
    setViewProfileDict: setViewProfileDict,
    setReceivedMessages: setReceivedMessages,
    setSentMessages: setSentMessages,
    //
    postProfileBySignup: postProfileBySignup,
    //
    formatDate: formatDate,
  };

  return (
    <ContextStore.Provider value={store}> {children} </ContextStore.Provider>
  );
};
export const useContextStore = () => useContext(ContextStore);
