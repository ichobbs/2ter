import React, { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useContextStore } from "./ContextStore";
import Schedule from "./Schedule";
import Bio from "./Bio";
import Notebook from "./Notebook";
import RequestedAppointments from "./RequestedAppointments";
import Reviews from "./Reviews";
import ReviewForm from "./ReviewForm";
import SendMessage from "./SendMessage";
import { useNavigate } from "react-router-dom";
import { Button, Box, Container, Grid, Typography } from "@mui/material";

function ViewProfile() {
  const { viewProfileDict, myProfileDict } = useContextStore();
  const [isReviewModalOpen, setIsReviewModalOpen] = useState(false);

  const { token } = useAuthContext();
  const navigate = useNavigate();

  useEffect(() => {
    if (Object.keys(viewProfileDict).length === 0) {
      navigate("/profiles");
    }
  }, []);

  const isOwnProfile = myProfileDict.profile_id === viewProfileDict.profile_id;
  const isAuthenticated = !!token;

  return (
    <Container>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        {/* Header */}

        <Box justifyContent="center">
          <Typography align="center" variant="h2">
            {viewProfileDict?.first_name} {viewProfileDict?.last_name}
          </Typography>

          <Typography align="center" variant="h3">
            Someone Else's Info
          </Typography>
        </Box>

        <Box sx={{ justifyContent: "center" }}>
          <Grid container spacing={2} justifyContent="center">
            {/* Row 1 */}
            <Grid item lg={5} md={8} s={10} xs={12}>
              <Schedule />
            </Grid>

            <Grid item lg={7} md={8} s={10} xs={12}>
              <Bio />
            </Grid>

            {/* Row 2 */}

            <Grid item lg={4} md={5} s={6} xs={12}>
              <RequestedAppointments />
            </Grid>

            <Grid item lg={4} md={5} s={6} xs={12}>
              <SendMessage />
            </Grid>

            <Grid item lg={4} md={5} s={6} xs={12}>
              <Reviews />
              {isAuthenticated && !isOwnProfile && (
                <Button
                  size="small"
                  variant="contained"
                  color="secondary"
                  onClick={() => setIsReviewModalOpen(true)}
                  sx={{ margin: 1, fontSize: "0.7rem" }}
                >
                  Write a Review
                </Button>
              )}
            </Grid>
          </Grid>
        </Box>
        {isReviewModalOpen && (
          <ReviewForm handleClose={() => setIsReviewModalOpen(false)} />
        )}
      </motion.div>
    </Container>
  );
}

export default ViewProfile;
