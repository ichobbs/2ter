import { useLocation } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { useContextStore } from "./ContextStore";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Box,
  Button,
  Modal,
  TextField,
} from "@mui/material";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

function RequestedAppointments() {
  const {
    getMyProfileRequestedEvents,
    getViewProfileRequestedEvents,
    requestedEvents,
    viewProfileDict,
    myProfileDict,
    request,
    urls,
    formatDate,
  } = useContextStore();
  const { token } = useAuthContext();
  const location = useLocation();
  const [modalOpen, setModalOpen] = useState(false);
  const [formData, setFormData] = useState({
    topic: "",
    author: myProfileDict.profile_id,
    partner: viewProfileDict.profile_id,
    paired: false,
    expired: false,
    created_at: new Date().toISOString(),
    scheduled_at: "",
    zoom_link: "",
    category: "",
  });

  const selectProfileID = (path) => {
    if (path === "/my_profile") {
      getMyProfileRequestedEvents();
    } else {
      getViewProfileRequestedEvents();
    }
  };

  const isOwnProfile = myProfileDict.profile_id === viewProfileDict.profile_id;
  const isAuthenticated = !!token;

  useEffect(() => {
    selectProfileID(location.pathname);
  }, [location.pathname, viewProfileDict]);

  const handleRequestClick = () => setModalOpen(true);
  const handleCloseModal = () => setModalOpen(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    if (name === "scheduled_at") {
      setFormData({
        ...formData,
        [name]: new Date(value),
      });
    } else {
      setFormData({
        ...formData,
        [name]: value,
      });
    }
  };

  const resetSubmit = () => {
    setModalOpen(false);
    setFormData({
      ...formData,
      scheduled_at: "",
    });
    console.log("Request submitted successfully");
    selectProfileID(location.pathname);
  };

  const resetPaired = () => {
    selectProfileID(location.pathname);
  };

  const handleRequestSubmit = async () => {
    try {
      await request.post(urls.events, formData, resetSubmit);
    } catch (error) {
      console.error("Error submitting request:", error);
    }
  };

  const handlePairEvent = async (event_id) => {
    const data = {
      event_id: event_id,
    };
    const event = await request.put(urls.event(event_id), data, resetPaired);
  };

  const [selectedAuthor, setSelectedAuthor] = useState("");
  const [selectedTopic, setSelectedTopic] = useState("");
  const [messageSubject, setMessageSubject] = useState("");
  const [messageBody, setMessageBody] = useState("");

  const [messageModalOpen, setMessageModalOpen] = useState(false);
  const handleSendMessageClick = (author) => {
    setSelectedAuthor(author);
    setMessageModalOpen(true);
  };
  const handleSelectedTopic = (topic) => {
    setSelectedTopic(topic);
  };
  const handleCloseMessageModal = () => setMessageModalOpen(false);

  const messageData = {
    sender: myProfileDict.profile_id,
    receiver: selectedAuthor,
    date: Date.now(),
    subject: messageSubject,
    body: messageBody,
  };

  const postNewMessage = async (messageData) => {
    try {
      await request.post(urls.messages, messageData, handleCloseMessageModal);
    } catch (error) {
      console.error("Error in sending message: ", error);
    }
  };

  const reset = () => {
    selectProfileID(location.pathname);
  };

  const handleExpireEvent = async (event_id) => {
    const data = {
      event_id: event_id,
    };
    console.log(data);
    const event = await request.put(urls.eventExpire(event_id), data, reset);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Box
        height="400px"
        sx={{
          p: 2,
          border: "1px solid black",
          backgroundColor: "primary.dark",
        }}
      >
        <Card
          sx={{
            p: 2,
            border: "1px solid black",
            backgroundColor: "primary.main",
            height: "100%",
            overflowY: "scroll",
          }}
        >
          <CardHeader
            title="Requested Appointments"
            sx={{ textAlign: "center" }}
          />

          <Modal
            open={modalOpen}
            onClose={handleCloseModal}
            aria-labelledby="modal-title"
            aria-describedby="modal-description"
          >
            <Box
              sx={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                width: 400,
                bgcolor: "background.paper",
                boxShadow: 24,
                p: 4,
              }}
            >
              <h2 id="modal-title">Request Event</h2>
              <TextField
                label="Topic"
                name="topic"
                value={formData.topic}
                onChange={handleInputChange}
                fullWidth
                sx={{ marginBottom: 2 }}
              />
              <DateTimePicker
                label="Scheduled Time"
                name="scheduled_at"
                value={formData.scheduled_at}
                onChange={(value) =>
                  handleInputChange({ target: { name: "scheduled_at", value } })
                }
                fullWidth
                renderInput={(params) => <DateTimePicker {...params} />}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={handleRequestSubmit}
                sx={{ marginTop: 2, marginLeft: 1 }}
              >
                Submit
              </Button>
            </Box>
          </Modal>

          <CardContent>
            <ul style={{ listStyleType: "none", padding: 0 }}>
              {requestedEvents?.map((event) => (
                <li key={event.event_id}>
                  <Card
                    variant="outlined"
                    sx={{ backgroundColor: "lightgray", mb: 2 }}
                  >
                    <CardHeader title={`Topic: ${event.topic}`} />
                    <CardContent>
                      <Typography variant="subtitle1">
                        Requested by: {event.author_first_name}{" "}
                        {event.author_last_name}
                      </Typography>
                      <Typography variant="body1">
                        Submitted: {formatDate(event.created_at)}
                      </Typography>
                      <Typography variant="body1">
                        For: {formatDate(event.scheduled_at)}
                      </Typography>
                      <Modal
                        open={messageModalOpen}
                        onClose={handleCloseMessageModal}
                        aria-labelledby={`message-modal-${event.event_id}`}
                        aria-describedby="modal-description"
                      >
                        <Box
                          sx={{
                            position: "absolute",
                            top: "50%",
                            left: "50%",
                            transform: "translate(-50%, -50%)",
                            width: 400,
                            bgcolor: "lightgray",
                            boxShadow: 24,
                            p: 4,
                          }}
                        >
                          <h2 id="message-modal">{`Re: ${selectedTopic}`}</h2>
                          <TextField
                            label="Subject"
                            name="Subject"
                            value={messageSubject}
                            onChange={(event) =>
                              setMessageSubject(event.target.value)
                            }
                            fullWidth
                            sx={{
                              backgroundColor: "white",
                              marginBottom: 2,
                            }}
                          />
                          <TextField
                            label="Message Body"
                            name="message_body"
                            fullWidth
                            multiline
                            rows="7"
                            variant="outlined"
                            value={messageBody}
                            onChange={(event) =>
                              setMessageBody(event.target.value)
                            }
                            sx={{
                              backgroundColor: "white ",
                              marginBottom: 2,
                            }}
                          />
                          <Button
                            size="small"
                            variant="contained"
                            color="secondary"
                            onClick={() => postNewMessage(messageData)}
                            sx={{
                              marginTop: 1,
                              marginLeft: 1,
                              marginBottom: 1,
                              fontSize: "0.7rem",
                            }}
                          >
                            Send
                          </Button>
                        </Box>
                      </Modal>
                      {location.pathname === "/my_profile" ? (
                        <div>
                          <Button
                            size="small"
                            variant="contained"
                            color="secondary"
                            onClick={() => {
                              handleSendMessageClick(event.author);
                              handleSelectedTopic(event.topic);
                            }}
                            sx={{
                              marginTop: 1,
                              marginLeft: 1,
                              marginBottom: 1,
                              marginRight: 3,
                              fontSize: "0.7rem",
                            }}
                          >
                            Send Message
                          </Button>
                          <Button
                            size="small"
                            variant="contained"
                            color="secondary"
                            onClick={() => handlePairEvent(event.event_id)}
                            sx={{
                              marginTop: 1,
                              marginLeft: 1,
                              marginBottom: 1,
                              fontSize: "0.7rem",
                            }}
                          >
                            Accept
                          </Button>
                          <Button
                            size="small"
                            variant="contained"
                            color="secondary"
                            onClick={() => handleExpireEvent(event.event_id)}
                            sx={{
                              fontSize: "0.7rem",
                              marginTop: 1,
                              marginLeft: 4,
                            }}
                          >
                            Decline
                          </Button>
                        </div>
                      ) : (
                        <div></div>
                      )}
                    </CardContent>
                  </Card>
                </li>
              ))}
            </ul>
          </CardContent>
        </Card>
      </Box>
      {!isOwnProfile && isAuthenticated && (
        <Button
          size="small"
          variant="contained"
          color="secondary"
          onClick={() => handleRequestClick()}
          sx={{ margin: 1, fontSize: "0.7rem" }}
        >
          Request Event
        </Button>
      )}
    </LocalizationProvider>
  );
}

export default RequestedAppointments;
