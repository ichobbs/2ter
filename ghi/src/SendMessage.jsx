import React, { useState } from "react";
import { useContextStore } from "./ContextStore";
import { Card, CardHeader, Box, TextField, Button, Modal } from "@mui/material";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

function SendMessage() {
  const { viewProfileDict, myProfileDict, request, urls } = useContextStore();
  const { token } = useAuthContext();

  const [messageSubject, setMessageSubject] = useState("");
  const [messageBody, setMessageBody] = useState("");

  const messageData = {
    sender: myProfileDict.profile_id,
    receiver: viewProfileDict.profile_id,
    date: Date.now(),
    subject: messageSubject,
    body: messageBody,
  };

  const [messageSent, setMessageSent] = useState(false);
  const handleCloseModal = () => setMessageSent(false);

  const successMessage = () => {
    setMessageSent(true);
    const delay = 2000;
    const timerID = setTimeout(() => {
      setMessageSent(false);
    }, delay);
    return () => {
      clearTimeout(timerID);
    };
  };

  const isOwnProfile = myProfileDict.profile_id === viewProfileDict.profile_id;
  const isAuthenticated = !!token;

  const postNewMessage = async (messageData) => {
    try {
      await request.post(urls.messages, messageData, successMessage);
    } catch (error) {
      console.error("Error in sending message: ", error);
    }
  };

  return (
    <div>
      <Box
        height="400px"
        sx={{
          p: 2,
          border: "1px solid black",
          backgroundColor: "primary.dark",
        }}
      >
        <Card
          sx={{
            p: 2,
            border: "1px solid black",
            backgroundColor: "primary.main",
            height: "100%",
          }}
        >
          <CardHeader
            title={`Message ${viewProfileDict.first_name}`}
            sx={{ textAlign: "center" }}
          />
          <TextField
            label="Subject"
            name="Subject"
            value={messageSubject}
            onChange={(event) => setMessageSubject(event.target.value)}
            fullWidth
            sx={{ backgroundColor: "lightgray", marginBottom: 2 }}
          />
          <TextField
            label="Message Body"
            name="message_body"
            fullWidth
            multiline
            rows="7"
            variant="outlined"
            value={messageBody}
            onChange={(event) => setMessageBody(event.target.value)}
            sx={{ backgroundColor: "lightgray", marginBottom: 2 }}
          />
        </Card>
      </Box>
      {!isOwnProfile && isAuthenticated && (
        <Button
          size="small"
          variant="contained"
          color="secondary"
          onClick={() => postNewMessage(messageData)}
          sx={{
            marginTop: 1,
            marginLeft: 1,
            marginBottom: 1,
            fontSize: "0.7rem",
          }}
        >
          Send
        </Button>
      )}
      <Modal
        open={messageSent}
        onClose={handleCloseModal}
        aria-labelledby="modal-title"
        aria-describedby="modal-description"
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 400,
            bgcolor: "background.paper",
            boxShadow: 24,
            p: 4,
          }}
        >
          <h2 id="modal-title">Message Sent</h2>
        </Box>
      </Modal>
    </div>
  );
}

export default SendMessage;
