import { useLocation } from "react-router-dom";
import React, { useEffect } from "react";
import { useContextStore } from "./ContextStore";
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Box,
  Rating,
} from "@mui/material";

function formatDate(isoDate) {
  const options = { year: "numeric", month: "long", day: "numeric" }; 
  return new Date(isoDate).toLocaleDateString(undefined, options); 
}

function calculateAverageRating(reviews) {
  if (reviews.length === 0) {
    return 0;
  }

  const totalRating = reviews.reduce((sum, review) => sum + review.rating, 0);
  const averageRating = totalRating / reviews.length;

  return averageRating.toFixed(2);
}

function Reviews() {
  const {
    myProfileDict,
    viewProfileDict,
    setReviewee,
    reviewsByReviewee,
    getReviewsByReviewee,
    reviewee,
  } = useContextStore();
  const location = useLocation();

  useEffect(() => {
    setReviewee(
      location.pathname === "/my_profile"
        ? myProfileDict.profile_id
        : viewProfileDict.profile_id
    );
  }, []);

  useEffect(() => {
    const delay = 500;
    const timerID = setTimeout(() => {
      getReviewsByReviewee();
    }, delay);
    return () => {
      clearTimeout(timerID);
    };
  }, [reviewee]);

  const averageRating = parseFloat(calculateAverageRating(reviewsByReviewee));

  return (
    <Box  height="400px"
      sx={{
        p: 2,
        border: "1px solid black",
        backgroundColor: "primary.dark",
      }}>
      <Card
        sx={{
          p: 2,
          border: "1px solid black",
          backgroundColor: "primary.main",
          height: "100%",
          overflowY: "scroll",
        }}
      >
        <CardHeader title="Reviews" sx={{ textAlign: "center" }} />
        <CardContent>
          <Typography variant="h6">
            Average Rating:{" "}
            <Rating name="read-only" value={averageRating} readOnly />
          </Typography>
          <ul style={{ listStyleType: "none", padding: 0 }}>
            {reviewsByReviewee?.map((review) => (
              <li key={review.review_id}>
                <Card
                  variant="outlined"
                  sx={{ backgroundColor: "lightgray", mb: 2 }}
                >
                  <CardHeader title={`Author: ${review.author}`} />
                  <CardContent>
                    <Typography variant="subtitle1">
                      Date: {formatDate(review.date)}{" "}
                    </Typography>
                    <Typography variant="body1">
                      <Rating name="read-only" value={review.rating} readOnly />
                    </Typography>
                    <Typography variant="body1">
                      Review: {review.review_text}
                    </Typography>
                  </CardContent>
                </Card>
              </li>
            ))}
          </ul>
        </CardContent>
      </Card>
    </Box>
  );
}

export default Reviews;
