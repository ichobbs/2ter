import React, { useEffect } from "react";
import { motion } from "framer-motion";
import { useContextStore } from "./ContextStore";
import Schedule from "./Schedule";
import Bio from "./Bio";
import Notebook from "./Notebook";
import RequestedAppointments from "./RequestedAppointments";
import Reviews from "./Reviews";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

function Me() {
  const { myProfileDict } =
    useContextStore();

  return (
    <Container>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        {/* Header */}

        <Box justifyContent="center">
          <Typography align="center" variant="h2">
            {myProfileDict?.first_name} {myProfileDict?.last_name}
          </Typography>

          <Typography align="center" variant="h3">
            A place to see your own information
          </Typography>
        </Box>

        <Box sx={{justifyContent: 'center'}}>
          <Grid
            container
            spacing={2}
            justifyContent="center"
          >
            {/* Row 1 */}
              <Grid item lg={5} md={8} s={10} xs={12}>
                <Schedule />
              </Grid>

              <Grid item lg={7} md={8} s={10} xs={12}>
                <Bio />
              </Grid>

              {/* Row 2 */}

              <Grid item lg={4} md={5} s={6} xs={12}>
                <RequestedAppointments />
              </Grid>

              <Grid item lg={4} md={5} s={6} xs={12}>
                <Reviews />
              </Grid>

              <Grid item lg={4} md={5} s={6} xs={12}>
                <Notebook />
              </Grid>
          </Grid>
        </Box>
      </motion.div>
    </Container>
  );
}

export default Me;
