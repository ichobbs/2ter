import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { AppBar, Toolbar, Typography, Button, Box } from "@mui/material";
import { useContextStore } from "./ContextStore";

function Nav() {
  const { getReviewsByReviewee } = useContextStore();

  const [account] = useState("");
  const { token } = useAuthContext();
  const { logout } = useToken();

  const handleLogout = async () => {
    try {
      await logout();
    } catch (error) {
      console.log(error);
    }
  };

  const handleMyProfileClick = () => {
    getReviewsByReviewee();
  };

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography
          variant="h6"
          component={NavLink}
          to="/profiles"
          color="inherit"
        >
          Profiles
        </Typography>
        {!token ? (
          <Box marginLeft="auto">
            <Button component={NavLink} to="/signup" color="inherit">
              Signup
            </Button>
            <Button component={NavLink} to="/login" color="inherit">
              Login
            </Button>
          </Box>
        ) : (
          <Box marginLeft="auto">
            <Button
              component={NavLink}
              to="/my_profile"
              color="inherit"
              onClick={handleMyProfileClick}
            >
              My Profile
            </Button>
            <Button component={NavLink} to="/messages" color="inherit">
              Messages
            </Button>
            <Button
              component={NavLink}
              to="/login"
              color="inherit"
              onClick={handleLogout}
            >
              Logout
            </Button>
          </Box>
        )}
      </Toolbar>
    </AppBar>
  );
}

export default Nav;
