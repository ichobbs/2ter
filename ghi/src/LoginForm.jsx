import React, { useState } from "react";
import { motion } from "framer-motion";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import { useContextStore } from "./ContextStore";

const LoginForm = () => {
  const { getAccountByLoginUsername, getMyProfileDict } = useContextStore();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login } = useToken();
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {

      await login(username, password);
      event.target.reset();
      const account = await getAccountByLoginUsername(username);

      await getMyProfileDict(account);

      navigate("/profiles");
    } catch (err) {
      console.error("Error during login: ", err);
    }
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <div className="offset-3 col-6">
        <div className="card">
          <h1 className="offset-3">Login</h1>
          <form className="form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                style={{ backgroundColor: "white", color: "#662d91" }}
                id="floatingInput"
                type="text"
                className="form-control"
                onChange={(event) => setUsername(event.target.value)}
                placeholder="username"
              />
              <label htmlFor="floatingInput">Username:</label>
            </div>
            <div className="form-floating mb-3">
              <input
                style={{ backgroundColor: "white", color: "#662d91" }}
                id="floatingPassword"
                type="password"
                className="form-control"
                onChange={(event) => setPassword(event.target.value)}
                placeholder="Password"
              />
              <label htmlFor="floatingPassword">Password:</label>
            </div>
            <div>
              <button type="submit" className="btn btn-primary">
                Login
              </button>
            </div>
          </form>
        </div>
      </div>
    </motion.div>
  );
};

export default LoginForm;
