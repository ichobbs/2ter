Team Members:

[Felix Mogire] (https://gitlab.com/felixmogire)


Project Description
Welcome to 2ter! This application is designed to help student connect to teachers for homework help, lessons and couching. The application will allow users to create an account, make an appointment, make a review and search a specific skill or role.
We have two frameworks that we are using for this application. The first is React, which is a JavaScript library for building user interfaces. The second is FastAPI, which is a modern, fast (high-performance), web framework for building APIs with Python 3.6+ based on standard Python type hints.
In 2ter we will be using a PostgreSQL database. PostgreSQL is a powerful, open source object-relational database system with over 30 years of active development that has earned it a strong reputation for reliability, feature robustness, and performance.

2ter diagram

![Alt text](EXCAL.png)


How to Run this App

Getting Started
Follow the instructions below to get this project up and running on your local machine for development and testing purposes.Please make sure you have access to Docker Desktop application and your terminal.

In your terminal

Change your directory to a folder you would  you would like to clone project to.
Run the following command to clone the project to your local machine.


git clone https://gitlab.com/Kgsalamander/2ter.git



Change your directory to the project folder.


cd 2ter




Run the following command to set up docker(create volumes for the data, build images, build containers).


docker volume create DataBucket
docker volume create pg-admin
docker-compose build
docker-compose up



When you run docker-compose up and if you're on macOS, you will see a warning about an environment variable named OS being missing. You can safely ignore this.
Type in your browser: http://localhost:3000/

You should now see the main page of the application with a navigation at the top of the page


Design

API design
Data model
GHI
